var util = require("./utilities");
var processes = require("./processes");
var globals = require("./globals");

outlets = 3;

var toDict = function (obj, name) {
  var dict = new Dict(name);
  dict.parse(JSON.stringify(obj));
};

var RULES_ENUM = "F" | "+";

/**
 *
 * @param {number} freq
 * @param {number} amp
 * @param {number} dur
 */
var SoundProps = function (freq, amp, dur, rot, note) {
  var s = this;
  s.freq = freq;
  s.amp = amp;
  s.dur = dur;
  s.pan = 0.5;
  s.rot = rot;
  s.note = note;
  s.reversed = false;

  s.orgfreq = freq;
  s.orgamp = amp;
  s.orgdur = dur;
  s.orgrot = rot;
  s.orgnote = note;
  s.orgpan = 0.5;
};

/* var branchProcess = function(name, direction, ){

} */

/**
 *
 * @param {number} id
 * @param {number} level
 * @param {TreeNode[] | string[]} children
 * @param {string} ruleValue
 * @param {SoundProps} soundProps
 */
var TreeNode = function (
  id,
  level,
  children,
  ruleValue,
  angle,
  soundProps,
  ctrlProps
) {
  var n = this;
  n.id = id;
  n.parrentId = 0;
  n.level = level;
  n.ruleValue = ruleValue;
  n.angle = angle;
  n.soundProps = soundProps;
  n.ctrlProps = ctrlProps;
  n.children = children;
  n.process = { glide: { finished: 0 }, playable: 0, skip: false };
};

var pelog = [
  134.4149,
  268.82979,
  537.65958,
  672.07448,
  806.48937,
  940.90427,
  1209.73406,
];

var ptrans = 8.0648937;

var pelog = [
  1.344149,
  2.6882979,
  5.3765958,
  6.7207448,
  8.0648937,
  9.4090427,
  12.0973406,
  1.344149 + ptrans,
  2.6882979 + ptrans,
  5.3765958 + ptrans,
  6.7207448 + ptrans,
  8.0648937 + ptrans,
  9.4090427 + ptrans,
  12.0973406 + ptrans,
];

//pelog = [1, 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];

var ONE_CENT = 1.0005777895065548592967925757932;

util.unsort(pelog, 5000);

var PLG = pelog.length;

var pelogRev = pelog.slice();
pelogRev = pelogRev.reverse();

var NOTES = [67, 72, 70, 69, 66, 62, 64, 66, 67];
var NL = NOTES.length;

var getMidiNote = function (id){
  var d = new Dict('lsys_midi');
  var notes = d.get('freqs');
  var durs = d.get('durs');
  return [notes[id%notes.length],durs[id%durs.length]];
}

var makeNodeNote = function (id, levelCounter, level, scale) {
  if (!scale) scale = pelog;
  return 10 + pelog[levelCounter % PLG];
};

var makeNodeFrequency = function (id, parrentConstant, level) {
  
  var note = pelog[id%PLG] * 100*(level+1);
   var trans = Math.pow(ONE_CENT, note);
   var f = 50* trans;
   if(f>3500) f = f / 2;
   return f + util.getRandomInt(f * 0.1);

   var f =
    50 +
    (Math.log(id + 1) * (level + 1) + Math.log(parrentConstant + 2) * level) *
      30;
  if(f>3500) f = f / 2;
  return f + util.getRandomInt(f * 0.1);
};

/**
 * Builds tree from LSystem string
 * @param {string[]} rules
 * @param {number} level
 * @param {{id:number}} idIndex
 * @param {TreeNode[]} tree
 * @param {number} angle
 * @param {{c:number}}rotCount
 * @param {{}}ctrlProps
 * @returns TreeNode[]
 */
var makeTree = function (
  rules,
  level,
  idIndex,
  tree,
  angle,
  rotCount,
  ctrlProps
) {
  var levelCounter = 0;
  var parrentConstant = idIndex.id;
  var BASE_DURATION = 1500;
  var DURATION_STEP = BASE_DURATION / (ctrlProps.maxLevel + 1);
  var cp = {};
  cp["dur"] = 500; //+ util.getRandomInt(500);
  cp["orgdur"] = cp["dur"];

  tree = new Array();

  while (rules.length > 0) {
    switch (rules[0]) {
      case "F": {
        idIndex.id++;
        var freq = makeNodeFrequency(idIndex.id, parrentConstant, level);
        var sp = new SoundProps(freq, 0.7);
        sp.note = makeNodeNote(idIndex.id, levelCounter, level);

        var scp = {};
        scp["dur"] =
          BASE_DURATION -
          DURATION_STEP * (level + 1) +
          util.getRandomInt(DURATION_STEP * (level + 1) * 0.5);

        // scp["dur"] = cp["dur"];
        scp["orgdur"] = scp["dur"];

        var node = new TreeNode(idIndex.id, level, [], "F", angle, sp, scp);

        node["parrentId"] = parrentConstant;
        tree.push(node);
        break;
      }

      case "+": {
        idIndex.id++;
        if (rotCount.c < 10000) rotCount.c++;

        var freq = makeNodeFrequency(idIndex.id, parrentConstant, level);

        var sp = new SoundProps(freq, 0.4);
        sp.note = makeNodeNote(idIndex.id, levelCounter, level);
        var cp = {};
        cp["dur"] = 50 + util.getRandomInt(100)+200;
        cp["orgdur"] = cp["dur"];
        var node = new TreeNode(idIndex.id, level, [], "+", angle, sp, cp);

        node["parrentId"] = parrentConstant;
        tree.push(node);

        break;
      }

      case "-": {
        idIndex.id++;
        if (rotCount.c > -10000) rotCount.c--;

        var freq = makeNodeFrequency(idIndex.id, parrentConstant, level);

        var sp = new SoundProps(freq, 0.3);
        //sp.note = 24 + levelCounter + 12 * level;
        sp.note = makeNodeNote(idIndex.id, levelCounter, level);
        var cp = {};
        cp["dur"] = 60 + util.getRandomInt(100)+500;
        cp["orgdur"] = cp["dur"];
        var node = new TreeNode(idIndex.id, level, [], "-", angle, sp, cp);
        node["parrentId"] = parrentConstant;
        tree.push(node);
        break;
      }
      case "[": {
        rules.shift();

        idIndex.id++;
        var cp = {};
        cp["dur"] = 2;// + util.getRandomInt(1000);
        cp["orgdur"] = cp["dur"];
        var node = new TreeNode(idIndex.id, level, [], "[", angle, {}, cp);
        node["parrentId"] = parrentConstant;
        tree.push(node);

        tree[tree.length - 1]["children"] = makeTree(
          rules,
          level + 1,
          idIndex,
          null,
          angle,
          rotCount,
          ctrlProps
        );

        idIndex.id++;

        var node = new TreeNode(idIndex.id, level, [], "]", angle, {}, cp);
        node["parrentId"] = parrentConstant;
        tree.push(node);

        break;
      }

      case "]":
        return tree;
    }

    levelCounter++;
    rules.shift();
  }
  return tree;
};
{
  var someRule = "F[F]FF[F[FFF]]F";
  //var someRule = "F[F[F]]F";
  var someRule = "FF+[+F-F-F]-[-F+F+F]";
  var someRule =
    "F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F+[-F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F]+F+F+F-F-F-F+[-F+F]+FF-FF+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F";
  var someRule = "F[+F+F]+[-F+F]+[+F-F+F+F-F]";
  //var tree = makeTree(R, 0, -1, tree, 10);

  /** prehlada tree (pole) a vrati vsetky indexy */
}


/**
 * Spravi z vnorenych stromov 1D pole (objekt) na seba odkazovanych nodov
 * @param {TreeNode[]} tree
 * @returns {TreeNode} flatTree
 */
var flattenTree = function (tree) {
  if (tree === undefined) return;

  var ALL_NODES_COUNT = tree.length;
  var breakPoint = 0;
  var queue = [];
  var flatTree = new Array();
  var flatTreeObj = {};
  var mainIndex = 0;

  for (var i = 0; i < tree.length; i++) {
    queue.push(tree[i]);
  }
  while (queue.length > 0) {
    if (breakPoint > ALL_NODES_COUNT * 10000) return;
    breakPoint = breakPoint + 1;
    var currentNode = queue[0];
    var nextNodeId = 0;

    //FIXME: zda sa ze este spravne na seba neodkazuju jednotlive nodes..
    // napr pridane nove childreny do queue ... neviem ako dopredu vediet ich id, mozno pomocou nejakeho dalsieho zasobniku

    if (queue.length > 1) {
      nextNodeId = queue[1].id;
    }

    if (currentNode === undefined) {
      continue;
    }
    // HERE DO SOMETHING

    //TODO: pridat ako node aj [ a ], [ bude normal node ukazovat na vsetky childreny, ] sa prida vzdy ako posledny children

    var childrenIDs = new Array();

    // pridat odkaz na children na nasledujuci node, ale iba pre level 1
    if (mainIndex < tree.length - 1) childrenIDs.push(nextNodeId);
    //childrenIDs.push(currentNode.level + "_" + nextNodeId);

    for (var i = currentNode.children.length - 1; i >= 0; i--) {
      queue.push(currentNode.children[i]);
      // childrenIDs.push(
      //   currentNode.children[i].level + "_" + currentNode.children[i].id
      // );
      childrenIDs.push(currentNode.children[i].id);
    }

    var newNode = new TreeNode(
      currentNode.id,
      currentNode.level,
      childrenIDs,
      currentNode.ruleValue,
      currentNode.angle,
      currentNode.soundProps,
      currentNode.ctrlProps
    );
    newNode.parrentId = currentNode.parrentId;
    //flatTree.push(newNode);
    //flatTreeObj[currentNode.level + "_" + currentNode.id] = newNode;
    flatTreeObj[currentNode.id] = newNode;
    mainIndex++;

    queue.shift();
  }
  post("\nDone!\nAll nodes count: " + breakPoint + "\n");
  return flatTreeObj;
};

var findNodeByID = function (tree, id) {
  if (tree.length < 1) return;
  for (var i = 0; i < tree.length; i++) {
    var t = tree[i];
    if (id === t.id) return t;
    var result = findNodeByID(tree[i].children, id);
    if (result) return result;
  }
};

var aBush = {
  axiom: "F",
  rules: {
    //F: "FF+[+F-F-F]-[-F+F+F]",
    F: "FF+[-F-F-F]-[-F+F+F]",
    //F: "FF[FFFFFF]-[FFFFF]",
  },
};

var genTreeFromLSystem = function () {
  post("\n\n\n------GENERATING------\n");
  outlet(0, "start");
  var args = arrayfromargs(arguments);
  var gen = args[0];
  var d = new Dict("lsys_dict");

  var rules = d.get("system::" + gen);

  var R = rules.split("");
  /**COMPUTE TREE ATRIBUTES */
  var ctrlProps = {};
  ctrlProps.nodesCount = R.length;
  ctrlProps.branchesCount = 0;
  ctrlProps.maxLevel = 0;

  var cl = 0;
  for (var i = 0; i < R.length; i++) {
    if (R[i] === "[") {
      ctrlProps.branchesCount++;
      cl++;
    }
    if (R[i] === "]") {
      if (ctrlProps.maxLevel < cl) ctrlProps.maxLevel = cl;
      cl--;
    }
  }
  post("\n", JSON.stringify(ctrlProps));
  var orgR = rules;
  var tree = makeTree(R, 0, { id: -1 }, tree, 20, { c: 0 }, ctrlProps);

  var flatTree = flattenTree(tree);

  //checkIntegrity(orgR, flatTree, 0);

  toDict({ tree: tree }, "lsys_tree_dict");
  toDict(flatTree, "lsys_flat_tree_dict");
  outlet(0, "done");
  post("\n------END------\n\n\n");
  createSortedBranches(5);

  
  //post(typeof gen4);
};

//var mainDict = new Dict("lsys_flat_tree_dict");

var allStacks = {};
var branchesStack = {};
var branchesStackLevels = {};
//toDict(allStacks, "lsys_multiStack");

var clearMultiStack = function () {
  allStacks = {};
};

var dumpMultiStack = function () {
  //toDict(allStacks, "lsys_multiStack");

  toDict(allStacks, "lsys_multiStack");
  toDict(branchesStack, "lsys_branchesStack");

  //post(JSON.stringify(allStacks));
};

var pushMultiStack = function () {
  //post("\n-------\n");
  var args = arrayfromargs(arguments);
  var id = args[0];
  //post(id);
  var d = new Dict("lsys_flat_tree_dict");
  var node = JSON.parse(d.get(id).stringify());

  // count not from -1 but from 0
  // pushnem childreny ktore su o level vyssie => su childreny tohto nodu
  var stackId = node.id;

  if (node.children.length > 1) {
    if (!allStacks[stackId]) allStacks[stackId] = new Array();

    // for (var i = node.children.length - 1; i > 0; i--)
    //   allStacks[stackId].push(node.children[i]);

    for (var i = 1; i < node.children.length; i++)
      allStacks[stackId].push(node.children[i]);
  }

  //pushnem posledny child tohto nodu, co je nasledujuci child v tom istom leveli
  stackId = node.parrentId;

  if (!allStacks[stackId]) allStacks[stackId] = new Array();
  if (node.children[0]) allStacks[stackId].push(node.children[0]);

  //post(JSON.stringify(allStacks));
};

var popMs = function (stack, sid) {
  if (!stack[sid] || stack[sid].length < 1) {
    //IF DEBUGGING, remove unused keys, for clarity
    stack[sid] = undefined;
    return;
  }
  var x = stack[sid].pop();

  return x;
};

// toto nastane vtedy, ked nejaky node dohral, ubehla mu dur, makeNote posiela 0
var popMultiStack = function () {
  var args = arrayfromargs(arguments);

  var id = args[0];
  var d = new Dict("lsys_flat_tree_dict");
  var node = JSON.parse(d.get(id).stringify());

  var p1 = undefined;
  var p2 = undefined;
  p2 = popMs(allStacks, node.parrentId);
  //post('\nPopping from ', node.parrentId, ' value: ', p2, ' \n');

  if (p2) {
    outlet(1, p2);
  } else {
    var olderNode;
    try {
      olderNode = JSON.parse(d.get(node.parrentId).stringify());
      p2 = popMs(allStacks, olderNode.parrentId);
      if (p2) outlet(1, p2);
    } catch (error) {
      //post("\nstack empty\n");
    }
  }

  if (node.children.length > 1) {
    p1 = popMs(allStacks, node.id);
    //post('\nPopping from ', node.id, ' value: ', p1, ' \n');
  }
  if (p1) outlet(1, p1);

  //post('\n ALLSTACKS: \n');
  //post(JSON.stringify(allStacks));
  return;
};

var sortBranch = function () {
  var args = arrayfromargs(arguments);
  var key = args[0];
  var minChildren = 3;

  post("\nsortedBranche  ", key, " \n");
  var tree = new Dict("lsys_flat_tree_dict");

  var branches = branchesStack;
  //create root node array
  branches[key] = new Array();

  var node = tree.get(key);
  var children = node.get("children");
  if (
    typeof children === "object" &&
    children !== null &&
    children.length > minChildren
  ) {
    branches[key] = children;

    branches[key].shift();
    branches[key].reverse();
  }
  // push root nodes ?? FIXME: TODO:
  //if (node.get("parrentId") === -1) branches["-1"].push(parseInt(keys[i]));

  branchesStack = branches;
  toDict(branches, "lsys_branch");
};

var createSortedBranches = function () {
  var args = arrayfromargs(arguments);
  var minChildren = args[0];
  if (!minChildren || minChildren < 3) minChildren = 3;
  post("\ncreateSortedBranches\n");
  var tree = new Dict("lsys_flat_tree_dict");
  var keys = tree.getkeys();
  var branches = {};
  //create root node array
  branches["-1"] = new Array();

  for (var i = 0; i < keys.length - 1; i++) {
    var node = tree.get(keys[i]);
    
    var children = node.get("children");
    if (
      typeof children === "object" &&
      children !== null &&
      children.length > minChildren
    ) {
      branches[keys[i]] = children;
      //remove fisrt chilrden -which is pointing to another branch actualy
      branches[keys[i]].shift();
      branches[keys[i]].reverse();
    }
    // push root nodes
    if (node.get("parrentId") === -1) branches["-1"].push(parseInt(keys[i]));
  }
  
  // count all playable notes and add them to tree nodes
  var k = Object.keys(branches);
  for (var i = 0; i < k.length - 1; i++) {
    
    var branch = branches[k[i]];
    var counter = 0;
    for (var j = 0; j < branch.length - 1; j++) {
      var nodeId = branch[j];
      
      var freq = tree.get(nodeId + "::soundProps::freq");

      if (typeof freq === "number") {
        counter++;
      }
    }

    for (var j = 0; j < branch.length - 1; j++) {
      var nodeId = branch[j];
      var freq = tree.get(nodeId + "::soundProps::freq");
      //tree.set(nodeId + "::soundProps::freq", 30*(j+1)*(i+1));

      if (typeof freq === "number") {
        var note = getMidiNote(j);
        tree.set(nodeId + "::soundProps::freq", note[0]);
        tree.set(nodeId + "::ctrlProps::dur", note[1]);
        tree.replace(nodeId + "::process::playable", counter);
      }
    }
  }

  toDict(branches, "lsys_branch");
  toDict(branches, "lsys_branch_org");
};

var createBrachesByLevel = function () {
  var args = arrayfromargs(arguments);
  var minChildren = args[0];
  var minLevel = args[1];
  if (!minChildren || minChildren < 3) minChildren = 3;
  if (!minLevel || minLevel < 0) minLevel = 0;
  post("create highest sorted branches\n");
  var tree = new Dict("lsys_flat_tree_dict");
  var keys = tree.getkeys();
  var branches = {};
  //create root node array

  for (var i = 0; i < keys.length - 1; i++) {
    var node = tree.get(keys[i]);
    var level = node.get("level");
    if (level < minLevel) continue;
    var children = node.get("children");
    if (
      typeof children === "object" &&
      children !== null &&
      children.length > minChildren
    ) {
      branches[keys[i]] = children;
      //remove fisrt chilrden -which is pointing to another branch actualy
      branches[keys[i]].shift();
      branches[keys[i]].reverse();
    }
    // push root nodes
  }
  branchesStackLevels = branches;
  toDict(branches, "lsys_branch_levels");
};

var createBranchById = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var tree = new Dict("lsys_flat_tree_dict");
  var keys = tree.getkeys();
  var branch = [];
  for (var i = 0; i < keys.length - 1; i++) {
    var node = tree.get(keys[i]);
    var pId = node.get("parrentId");
    if (pId === parrentId) branch.push(parseInt(keys[i]));
  }
  post(branch.length);
  //create object so i can make a dict from it
  var branchId = {};
  branchId[parrentId] = branch;
  toDict(branchId, "lsys_branch");
  post("\nDone.\n");
};

var activeBranches = {};
var branchActivate = function () {
  var args = arrayfromargs(arguments);
  var branchIds = args; // = parrentId[], same thing
  var newActiveBranches = {};
  var d = new Dict('lsys_active_branches');
  for (var i = 0; i < branchIds.length; i++) {
    var isActive = d.get(branchIds[i]);
    newActiveBranches[branchIds[i]] = true;
    if (!isActive) {
      d.replace(branchIds[i], true);
      branchPlayer(branchIds[i]);
    }
  }
   toDict(newActiveBranches, "lsys_active_branches");
  
};

var branchAdd = function () {
  var args = arrayfromargs(arguments);
  var branchId = args[0]; // = parrentId[], same thing
  var d = new Dict('lsys_active_branches');
  d.replace(branchId, true);  
  branchPlayer(branchId);
};

var branchRemove = function () {
  var args = arrayfromargs(arguments);
  var branchId = args[0];
  var d = new Dict('lsys_active_branches');
  var isActive = d.get(branchId);
  if(!isActive)return;
  d.remove(branchId.toString());
  
};

var branchPlayer = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var d = new Dict('lsys_active_branches');
  var isActive = d.get(parrentId);
  
  if (!isActive) return;
  var d = new Dict('lsys_branch');
  var branch = d.get(parrentId);
  if(!branch) return;

  //rotate array
  
    branch.push(branch.shift());
    d.replace(parrentId, branch);
    var nodeId = branch[0];
    outlet(1, ["doProcesses", parrentId, nodeId]);
  
};

var branchMetroTrig = function () {
  //FIXME: don use activeBranches and branchesStack, but dictionaries - lsys_active_branches and lsys_branch
  var keys = Object.keys(activeBranches);
  if (!keys) return;
  if (typeof keys === "object" && keys !== null) {
    for (var i = 0; i < keys.length; i++) {
      var parrentId = keys[i];
      if (branchesStack[parrentId]) {
        branchesStack[parrentId].push(
          branchesStack[parrentId].shift()
        );
        var nodeId = branchesStack[parrentId][0];
        outlet(1, ["doProcesses", parrentId, nodeId]);
        //outlet(2, nodeId);
      }
    }
  }
};

var parseAndApplyProcessDepthSearch = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var nodeId = args[1];
  var d = new Dict("lsys_processes");
  var proc = d.get(parrentId);
  if (!proc) return;
  var keys = d.get(parrentId).getkeys();
  if (!keys) return;

  if (typeof keys === "object" && keys !== null) {
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      applyProcess(nodeId, key, proc.get(key));
    }
  } else {
    var key = keys;
    applyProcess(nodeId, key, proc.get(key));
  }
};



var branchTempo = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var tempo = args[1];
  var tree = new Dict("lsys_flat_tree_dict");
  var branchesStack = new Dict('lsys_branch');
  var keys = branchesStack.get(parrentId);
  if (!keys) return;
  if (tempo < 0.01) tempo = 0.01;

  //tempo = tempo + util.getRandomInt(tempo*0.1);
  for (var i = 0; i < keys.length; i++) {
    var orgTempo = tree.get([keys[i]] + "::ctrlProps::orgdur");
    tree.set([keys[i]] + "::ctrlProps::dur", tempo * orgTempo);
  }
};

var oldbranchTempo = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var tempo = args[1];
  var tree = new Dict("lsys_flat_tree_dict");
  var keys = branchesStack[parrentId];
  if (!keys) return;
  if (tempo < 1) tempo = 1;

  //tempo = tempo + util.getRandomInt(tempo*0.1);
  for (var i = 0; i < keys.length; i++) {
    var orgTempo = tree.get([keys[i]] + "::ctrlProps::orgdur") / 100;
    tree.set([keys[i]] + "::ctrlProps::dur", tempo * orgTempo);
  }
};

var branchAmp = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var amp = args[1];
  var tree = new Dict("lsys_flat_tree_dict");
  var branchesStack = new Dict('lsys_branch');
  var keys = branchesStack.get(parrentId);
  
  if (!keys) {
    post();
    post(parrentId, " branch not found!");
    post();
    return;
  }
  
  if (amp > 0.99999999) amp = 0.999999;
  for (var i = 0; i < keys.length; i++) {
    tree.set(keys[i] + "::soundProps::amp", amp);
    //tree.set([keys[i]] + "::process",proc)
  }
};

var branchMagnetFreq = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var target = args[1];
  var steps = args[2];
  if (!steps || steps < 2) steps = 10;
  var tree = new Dict("lsys_flat_tree_dict");
  var branchesStack = new Dict('lsys_branch');
  var keys = branchesStack.get(parrentId);
  if (!keys) {
    post();
    post(parrentId, " branch not found!");
    post();
    return;
  }
  if (target < 0.000001) target = 0.000001;
  for (var i = 0; i < keys.length; i++) {
    var freq = tree.get([keys[i]] + "::soundProps::freq");
    if (!freq) continue;

    if (util.roughEqual(target, freq, 10)) continue;
    var orgfreq = tree.get([keys[i]] + "::soundProps::orgfreq");
    var oneStep = (target - orgfreq) / steps;
    40;
    freq = freq + oneStep;

    tree.set([keys[i]] + "::soundProps::freq", freq);
  }
};

var branchReset = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var branchesStack = new Dict('lsys_branch');
  var orgBranchesStack = new Dict('lsys_branch_org');
  var orgStack  = orgBranchesStack.get(parrentId);
  if (!orgStack) {
    post();
    post(parrentId, " branch not found!");
    post();
    return;
  }
  branchesStack.replace(parrentId, orgStack);
  var tree = new Dict("lsys_flat_tree_dict");
  var orgTree = new Dict("lsys_flat_tree_dict_org");
  var keys = orgStack;
  for (var i = 0; i < keys.length; i++) {
    var orgNode = orgTree.get(keys[i]);
    tree.replace(keys[i], orgNode);
  }
  
};

var searchTree = function (tree, level, ids) {
  if (tree.length < 1) return;

  for (var i = 0; i < tree.length; i++) {
    searchTree(tree[i].children, level + 1, ids);

    ids.push(tree[i].id);
  }
  return ids;
};

/** prehlada tree (objekt) a vrati vsetky indexy */
var deepSearchObjTree = function (tree, level, ids) {
  if (tree.length < 1) return;

  for (var i = 0; i < tree.length; i++) {
    ids.push(tree[i].ruleValue);
    deepSearchObjTree(tree[i].children, level + 1, ids);
  }
  return ids;
};

/** prehlada flatTree (objekt) a vrati pole vsetkych ruleValues */
var deepSearchObjFlatTree = function (tree, index, result) {
  if (!tree[index]) return;
  var stack = new Array();
  stack.push(index);
  while (stack.length > 0) {
    var node = tree[stack.pop()];
    result.push(node.ruleValue);
    for (var i = 0; i < node.children.length; i++) stack.push(node.children[i]);
  }

  return result;
};

/** prehlada flatTree (objekt) a vrati pole vsetkych indexov */
var deepSearchObjFlatTreeINDEXES = function (tree, index, result) {
  if (!tree[index]) return;
  var stack = new Array();
  stack.push(index);
  while (stack.length > 0) {
    index = stack.pop();

    var node = tree[index];
    result.push(index);

    for (var i = 0; i < node.children.length; i++) stack.push(node.children[i]);
  }

  return result;
};

var checkIntegrity = function (stringRule, flatTree, entryIndex) {
  var rules = new Array();
  //ids = deepSearchObjTree(tree,0,ids);
  rules = deepSearchObjFlatTree(flatTree, entryIndex, rules);

  var indexses = new Array();
  indexses = deepSearchObjFlatTreeINDEXES(flatTree, entryIndex, indexses);

  var joinedRules = rules.join("");

  //console.log(joinedRules);
  post("\nflatTree correctly indexed: ", joinedRules === stringRule);
  //console.log("flatTree correctly indexed: ", joinedRules === stringRule);

  if (joinedRules === stringRule) return;

  post("\nbugs:\n");
  post("\nJoined:\n");
  post(joinedRules);
  post("\nOrg:\n");
  post(stringRule);

  var duplicates = new Array();
  for (var i = 0; i < indexses.length; i++) {
    var ci = indexses[i];
    for (var j = i + 1; j < indexses.length; j++) {
      if (indexses[i] === indexses[j]) duplicates.push(indexses[i]);
    }
  }

  for (var i = 0; i < joinedRules.length; i++) {
    if (joinedRules[i] !== stringRule[i]) {
      post(
        "\ndiff in: " +
          i +
          " ...  " +
          joinedRules[i] +
          "  vs  " +
          stringRule[i] +
          "  " +
          indexses[i]
      );
    }
  }
};

/** ----------------OLD stuf-------------------------------- */

var bang = function () {
  //return 0;
  var someRule = util.generateLSystemString(4, aBush);
  //console.log(someRule);
  var rule = someRule.split("");
  var idIndex = { id: -1 };
  var tree = makeTree(rule, 0, { id: -1 }, tree, 20, { c: 0 });

  var flatTree = flattenTree(tree);

  checkIntegrity(someRule, flatTree, "0");

  //FIXME: odkomentovat - je to tu len koli node.js
  //toDict({ tree: tree }, "lsys_tree_dict");
  //toDict(flatTree, "lsys_flat_tree_dict");

  util.writeJson("flatTree.json", flatTree);
  util.writeJson("tree.json", tree);
};

//bang();

/**
 * MAX function, searchs in flatten tree
 */
var getNode = function () {
  var args = arrayfromargs(arguments);
  post("--");
  post(args);
  post("--");
  var id = args[0];

  var d = new Dict("lsys_flat_tree_dict");
  var val = d.get(id);
  var yval = JSON.parse(val.stringify());

  outlet(0, yval.id);
  post(yval.id);
};


