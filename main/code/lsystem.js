var util = require("./utilities");
var getRandomInt = util.getRandomInt;
var scaleNumber = util.scaleNumber;

var log = function (m) {
  post();
  post(m);
  post();
};

var out = function (m) {
  outlet(0, m);
};

var highTree = {
  axiom: "X",
  rules: {
    F: "FF",
    X: "F[+X]F[-X]+X",
  },
  Angle: "π/9",
};

var koch = {
  axiom: " F++F++F",
  rules: {
    F: "F-F++F-F",
  },
  angle: 60,
};


var julianTree = {
  axiom: "F+F+F",
  rules: {
    F: "F-[-F]+F",
  },
};

var glibertCurve = {
  axiom: "X",
  rules: {
    X: "-YF+XFX+FY-",
    Y: "+XF-YFY-FX+",
  },
};

var aBush = {
  axiom: "F",
  rules: {
    F: "FF+[+F-F-F]-[-F+F+F]",
  },
};

var bilina = {
  axiom: "F",
  rules: {
    //F: "F[+F]F[-F]F",
    //Katerinkin fraktalik
    F: "F+F+F-F-F-F+[-F+F]+FF-F",
  },
};

var deepTree = {
  axiom: "F",
  rules: {
    //F: "F[+F]F[-F]F",
    //Katerinkin fraktalik
    F: "F+F-F[F+F[F]F[F-F[F[F+F-F]F+F]F-F]F]F",
  },
};


var superDeep = {
  axiom: "F",
  rules: {
    //F: "F[+F]F[-F]F",
    //Katerinkin fraktalik
    F: "F[+F-[F--F[F[F[+[--[F[+F]]]]]]++F]-FF]+F",
  },
};


function applyRule(rules, char) {
  return rules[char] || char;
}


function renderAGeneration(system, previousGeneration, partial) {
  var nextGeneration = "";
  var startIndex = 0;
  if(partial) startIndex = Math.floor(10);
  for (var i = 0; i < previousGeneration.length; i++) {
    if(i>=startIndex || !partial)
      nextGeneration += applyRule(system.rules, previousGeneration[i])
    else nextGeneration+=previousGeneration[i];
  }
  return nextGeneration;
}

numIters = 8;
system = highTree;
//var x = F[+F-[F--F[F[F[+[--[F[+F]]]]]]++F]-FF]+F;

var applyCustomRuleType = function () {
  var args = arrayfromargs(arguments);
  var newTreeType = args[0];
  var d = new Dict("lsys_currentSystem");
  // uz driv vygenerovany l system
  var keys = new Dict("lsys_currentSystem").getkeys();
  var latestSystem = keys[keys.length-1];
 // var dd = new Dict(currentTree.stringify());
  //var cc = dd.getkeys();
  post(JSON.stringify(latestSystem));
  //post(JSON.parse(dd));
  var treeType = d.get(latestSystem);
  post(treeType);
  
  //post('Input: ',generations, rule, axiom);
  var system = deepTree;

}


var genLSystem = function () {
  var args = arrayfromargs(arguments);

  var generations = args[0];
  var rule = args[1];
  var axiom = args[2];
  post('Input: ',generations, rule, axiom);
  var system = deepTree;
  system.axiom = axiom;
  system.rules = { F: rule, X:'X+[F]-X' };
  var systemState = system.axiom;

  log(systemState);
  out(system);

  var dict = new Dict("lsys_dict");
  var all = {};
  for (var i = 1; i < generations; i++) {
    systemState = renderAGeneration(system, systemState, false);
    all["gen_" + i] = systemState;
  }

  dict.parse(JSON.stringify({ system: all }));
  out(JSON.stringify(all));
};

var makeBasicTree = function () {
  
  var args = arrayfromargs(arguments);
  var generations = args[0];
  if(!generations || generations < 1 || generations > 15)
    generations = 1;
  
  
  
  var system = highTree; 
  var systemState = system.axiom;
  post('\n---------Making LSystem---------\n')
  post('Input: ', generations, system.rules, system.axiom);
  log(systemState);
  out(system);

  var dict = new Dict("lsys_dict");

  var all = {};
  for (var i = 1; i < generations; i++) {
    systemState = renderAGeneration(system, systemState, false);
    all["gen_" + i] = systemState;
  }
  var dict2 = new Dict("lsys_currentSystem");
  dict2.parse(JSON.stringify( all));

  dict.parse(JSON.stringify({ system: all }));
  out(JSON.stringify(all));
};


//old
var bang = function () {
  var systemState = system.axiom;
  log(systemState);
  out(system);

  var dict = new Dict("lsys_dict");
  var all = {};
  for (var i = 1; i <= numIters; i++) {
     //if (i >= numIters - 2)
       systemState = renderAGeneration(system, systemState, false);
    // else systemState = renderAGeneration(aBush, systemState);

    //systemState = renderAGeneration(superDeep, systemState, i>4);
    all["gen_" + i] = systemState;
    // log(systemState);
  }

  // var genArray = new Array();
  // for(var i = 0; i<systemState.length; i++)
  //   genArray.push(systemState[i]);

  //var dictArray = new Dict('array_dict');
  //dictArray.parse(JSON.stringify({ system: genArray }));
  dict.parse(JSON.stringify({ system: all }));
  out(JSON.stringify(all));
};