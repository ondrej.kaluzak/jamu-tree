var util = require("./utilities");
outlets = 2;
var G = new Global("TREE_GLOBAL");
var GLOBAL_PROC = "GLOB";

var doProcesses = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var nodeId = args[1];
  //parseAndApplyGlobalProcess(parrentId, nodeId);

  parseAndApplyProcess(parrentId, nodeId);
  //FIXME: zacykli sa
  var tree = new Dict("lsys_flat_tree_dict");
  var skip = tree.get(nodeId + "::process::skip");
  if (skip) {
    outlet(1, ["branchPlayer", parrentId]);
    return;
  }
  outlet(0, nodeId);
};

var doProcessNoSkip = function () {
  var args = arrayfromargs(arguments);
  var parrentId = args[0];
  var nodeId = args[1];
  //parseAndApplyGlobalProcess(parrentId, nodeId);

  parseAndApplyProcess(parrentId, nodeId);
  //FIXME: zacykli sa

  outlet(0, nodeId);
};


//TODO: funguje, ale jednotlive procesy nie su pripravene na to ze parrentId (branch) je GLOB
var parseAndApplyGlobalProcess = function (parrentId, nodeId) {
  var d = new Dict("lsys_processes");
  var globalProc = d.get(GLOBAL_PROC);

  if (!globalProc) return;

  var keys = d.get(GLOBAL_PROC).getkeys();
  if (!keys) {
    //TODO: not shure if works
    d.remove(parrentId.toString());
    return;
  }

  if (typeof keys === "object" && keys !== null) {
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      applyProcess(nodeId, key, globalProc.get(key));
    }
  } else {
    var key = keys;
    applyProcess(nodeId, key, globalProc.get(key));
  }
};

var parseAndApplyProcess = function (parrentId, nodeId) {
  var d = new Dict("lsys_processes");
  var proc = d.get(parrentId);
  if (!proc) return;
  var keys = d.get(parrentId).getkeys();
  if (!keys) {
    d.remove(parrentId.toString());
    return;
  }

  if (typeof keys === "object" && keys !== null) {
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      applyProcess(nodeId, key, proc.get(key));
    }
  } else {
    var key = keys;
    applyProcess(nodeId, key, proc.get(key));
  }
};

/**
 *
 * @param {TreeNode} node
 * @param {object} process
 * @returns
 */
var applyProcess = function (nodeId, name, process) {
  var isActive = process.get("isActive");
  if (!isActive) return;

  if (name === "magnet") {
    var steps = process.get("steps");
    var target = process.get("target");
    var isCyclic = process.get("isCyclic");

    magnetProcess(nodeId, target, steps, isCyclic);
    return;
  }

  if (name === "amp") {
    var scale = process.get("scale");
    var steps = process.get("steps");
    var target = process.get("target");
    var isCyclic = process.get("isCyclic");
    ampProcess(nodeId, target, steps);
    return;
  }

  if (name === "cress") {
    var steps = process.get("steps");
    cresendoProcess(nodeId, steps);
    return;
  }

  if (name === "decress") {
    var factor = process.get("factor");
    decresendoProcess(nodeId, factor);
    return;
  }

  if (name === "glide") {
    var factor = process.get("factor");
    var fastMode = process.get("fastMode");
    glideProcess(nodeId, factor, fastMode);
    return;
  }

  if (name === "tempoGlide") {
    var factor = process.get("factor");
    tempoGlideProcess(nodeId, factor);
    return;
  }

  if (name === "reducer") {
    reducerProcess(nodeId);
    return;
  }

  if (name === "adder") {
    adderProcess(nodeId);
    return;
  }

  if (name === "gAmp") {
    // var scale = process.get("scale");
    // var steps = process.get("steps");
    // var target = process.get("target");
    // var isCyclic = process.get("isCyclic");
    ampProcessGlobal(nodeId);
    return;
  }
};
/**
 *
 * @param {number} nodeId
 */
var ampProcess = function (nodeId, target, steps) {
  if (!target || !steps) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var amp = tree.get(nodeId + "::soundProps::amp");
  if (!amp) return;
  if (amp > 0.99999999) amp = 0.999999;
  if (amp < 0.0001) amp = 1;
  tree.set(nodeId + "::soundProps::amp", G.G_AMP * amp);
};

var reducerProcess = function (nodeId) {
  //var skip = util.getRandomInt(100);
  //if (skip > 50) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var freq = tree.get(nodeId + "::soundProps::freq");
  //if(freq && util.getRandomInt(100)>50)return;
  var branch = tree.get(nodeId + "::parrentId");
  var procDict = new Dict("lsys_processes");

  var skipped = procDict.get(branch + "::reducer::skipped");
  var skippable = procDict.get(branch + "::reducer::skippable");
  var toSkip = procDict.get(branch + "::reducer::toSkip");

  if (skipped >= skippable - 2 || skipped > toSkip) {
    procDict.remove(branch + "::reducer");
    return;
  }
  procDict.replace(branch + "::reducer::skipped", skipped + 1);

  tree.replace(nodeId + "::process::skip", 1);

  return;
};

var adderProcess = function (nodeId) {
  if (!nodeId) return;
  var add = util.getRandomInt(100);
  if (add > 50) return;
  var tree = new Dict("lsys_flat_tree_dict");

  var branch = tree.get(nodeId + "::parrentId");
  if (!branch) return;
  var procDict = new Dict("lsys_processes");

  var added = procDict.get(branch + "::adder::added");
  var branchLength = procDict.get(branch + "::adder::branchLength");
  var toAdd = procDict.get(branch + "::adder::toAdd");
  if (added >= branchLength || added > toAdd) {
    procDict.remove(branch + "::adder");
    return;
  }
  procDict.replace(branch + "::adder::added", added + 1);

  tree.replace(nodeId + "::process::skip", 0);
  return;
};

var cresendoProcess = function (nodeId, steps) {
  if (!steps) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var orgamp = tree.get(nodeId + "::soundProps::orgamp");
  var amp = tree.get(nodeId + "::soundProps::amp");
  if (!orgamp) return;
  var oneStep = orgamp / steps;
  amp += oneStep;
  if (amp > orgamp) return;

  if (amp > 0.99999999) amp = 0.999999;
  if (amp < 0.0001) amp = 0.0001;

  tree.set(nodeId + "::soundProps::amp", amp);
};

//funguje aj ako cressendo s factorom > 1
var decresendoProcess = function (nodeId, factor) {
  if (!factor) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var amp = tree.get(nodeId + "::soundProps::amp");

  if (!amp) return;
  var branch = tree.get(nodeId + "::parrentId");
  var procDict = new Dict("lsys_processes");
  var oldFactor = procDict.get(branch + "::decress::factor");

  amp = amp * factor * oldFactor;
  if (amp > 0.99999999) amp = 0.999999;
  if (amp < 0.0001) amp = 0.0001;

  var dim = 1;
  if (oldFactor > 1) {
    dim = -1;
  }
  procDict.set(branch + "::decress::factor", oldFactor + 0.05);
  tree.set(nodeId + "::soundProps::amp", amp);
};

var tempoGlideProcess = function (nodeId, factor) {
  if (!factor) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var dur = tree.get(nodeId + "::ctrlProps::dur");
  if (!dur) return;
  dur = dur * factor;
  if (dur > 5000) dur = 5000;
  if (dur < 10) dur = 10;
  tree.set(nodeId + "::ctrlProps::dur", dur);
};

var glideProcess = function (nodeId, factor, fastMode) {
  if (!factor) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var playable = tree.get(nodeId + "::process::playable");
  if (typeof playable !== "number" || playable < 1) return;

  var isFinished = tree.get(nodeId + "::process::glide::finished");
  if (typeof isFinished === "number" && isFinished === 1) return;

  var freq = tree.get(nodeId + "::soundProps::freq");
  //if (!freq) return;

  var branch = tree.get(nodeId + "::parrentId");

  var procDict = new Dict("lsys_processes");

  freq = freq * factor;
  if (freq > 3500) {
    freq = freq / 1.5;
    tree.set(nodeId + "::process::glide::finished", 1);
    var finished = procDict.get(branch + "::glide::finished");
    finished++;
    if (finished >= playable) {
      post("remove process");
      procDict.remove(branch + "::glide");
      unlockBranch(branch, "glide");
      return;
    }
    procDict.replace(branch + "::glide::finished", finished);
  }
  if (freq < 50) {
    freq = freq*2;
    tree.set(nodeId + "::process::glide::finished", 1);
    var finished = procDict.get(branch + "::glide::finished");
    finished++;
    if (finished >= playable) {
      post("remove process");
      procDict.remove(branch + "::glide");
      unlockBranch(branch, "glide");
      return;
    }
    procDict.replace(branch + "::glide::finished", finished);
  }
  tree.set(nodeId + "::soundProps::freq", freq);
  //tree.replace(nodeId + "::process::glide::finished", 0);

  if (!fastMode) return;
  
  var oldFactor = procDict.get(branch + "::glide::factor");
  if (oldFactor > 2 || oldFactor < 0.001) {
    procDict.remove(branch + "::glide");
    unlockBranch(branch, "glide");
  } else {
    var dim = 1;
    if (oldFactor > 1) {
      dim = -1;
    }
    procDict.set(
      branch + "::glide::factor",
      oldFactor * factor + dim * oldFactor * 0.1
    );
  }

  //d.replace(branch+'::glide::finished', factor*oldFactor);
};

function unlockBranch(branch, proc) {
  var tree = new Dict("lsys_flat_tree_dict");
  var branches = new Dict("lsys_branch");
  var keys = branches.get(branch);

  for (var i = 0; i < keys.length; i++) {
    tree.set([keys[i]] + "::process::" + proc + "::finished", 0);
  }
}

var ampProcessGlobal = function (nodeId) {
  var tree = new Dict("lsys_flat_tree_dict");
  var amp = tree.get(nodeId + "::soundProps::orgamp");
  if (!amp) return;
  if (amp > 0.99999999) amp = 0.999999;
  if (amp < 0.0001) amp = 1;
  tree.set(nodeId + "::soundProps::amp", G.G_AMP * amp);
};

var magnetProcess = function (nodeId, target, steps, isCyclic) {
  if (!target || !steps || isCyclic === undefined) return;
  var tree = new Dict("lsys_flat_tree_dict");
  var freq = tree.get(nodeId + "::soundProps::freq");
  if (!freq) return;

  if (util.roughEqual(target, freq, 10) && isCyclic) {
    var reversed = tree.get(nodeId + "::soundProps::reversed");
    reversed = 1;
    tree.set(nodeId + "::soundProps::reversed", reversed);

    var orgfreq = tree.get(nodeId + "::soundProps::orgfreq");
    var oneStep = (orgfreq - freq) / steps;
    freq = freq + oneStep;
    tree.set(nodeId + "::soundProps::freq", freq);
    return;
  }
  var orgfreq = tree.get(nodeId + "::soundProps::orgfreq");

  if (util.roughEqual(orgfreq, freq, 10) && isCyclic) {
    var reversed = tree.get(nodeId + "::soundProps::reversed");
    reversed = 0;
    tree.set(nodeId + "::soundProps::reversed", reversed);

    var orgfreq = tree.get(nodeId + "::soundProps::orgfreq");
    var oneStep = (target - freq) / steps;
    freq = freq + oneStep;
    tree.set(nodeId + "::soundProps::freq", freq);
    return;
  }
  if (util.roughEqual(target, freq, 10) && !isCyclic) return;
  var reversed = tree.get(nodeId + "::soundProps::reversed");
  var oneStep = (target - orgfreq) / steps;
  if (reversed === 1 && isCyclic) {
    oneStep = (orgfreq - freq) / steps;
  }

  freq = freq + oneStep;
  tree.set(nodeId + "::soundProps::freq", freq);
};
