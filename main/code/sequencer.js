var util = require("./utilities");
var spread = util.spread;
outlets = 3;

var G = new Global("TREE_GLOBAL");
var paused = false;
var stopped = true;
G.SEQ = 0;
G.MAIN_TEMPO = 1;
var c = [];

var js = "JS";
var dict = "DICT";
var _self = "SELF";
var b = "branchAdd";
var r = "branchRemove";
var processes = "processes";
var bMagnet = "branchMagnetFreq";
var mgProc = "mgProc";
var bAmp = "branchAmp";
var cressProc = "decress";
var decressProc = "decress";
var glideProc = "glide";
var tempoGlideProc = "tempoGlide";

var msg = "MSG";
var wait = "WAIT";
var pauseMSG = [msg, "paused"];
var resumedMSG = [msg, "resumed"];
var stopMSG = [msg, "stopped"];
var startMSG = [msg, "started"];

var magnet = function (branch, freq) {
  if (freq === undefined) freq = 1000;
  return [dict, mgProc, branch, freq, 40, 1, 0];
};

// var cress = function (branch, steps) {
//   return [dict, cressProc, branch, steps, 1];
//};

var decress = function (branch, factor) {
  return [dict, decressProc, branch, factor, 1];
};
//FIXME: docasny flag cress = decress, lebo ako jedine vie oboje exponencialne
var cress = decress;

var glide = function (branch, factor) {
  return [dict, glideProc, branch, factor, 1, 0, 0];
};

var fastGlide = function (branch, factor) {
  if (factor === undefined)
    return [msg, "unable to set fastGlide, missing factor"];
  return [_self, "selfFastGlide", branch, factor];
};

var mute = function (branch) {
  return [js, bAmp, branch, 0];
};

var reducer = function (branch, toSkip) {
  return [_self, 'selfReducerProc', branch, toSkip];
};

var adder = function (branch, toAdd) {
  return [_self, 'selfAdderProc', branch, toAdd];
};

var calm = function (branch, amp) {
  if (amp === undefined) amp = 0.01;
  return [js, bAmp, branch, 0.01];
};

var removeProc = function (branch, proc) {
  return [_self, "selfRemoveProc", branch, proc];
};

var unlock = function (branch, proc) {
  return [_self, "selfUnlockBranch", branch, proc];
};

var resetBranch = function (branch) {
  if (branch === undefined) return [msg, "unable to reset branch"];
  return [js, "branchReset", branch];
};
//TODO: zistit ci funguje len takto bez 3. parametru
var stoppAll = function () {
  return [js, "branchActivate"];
};

var removeAllProc = function () {
  return [dict, processes, "clear"];
};

var removeAllBranchProc = function (branch) {
  return [_self, "selfRemoveAllBranchProc", branch];
};

var tempoGlide = function (branch, factor) {
  if (factor === undefined)
    return [msg, "unable to set tempoGlide, missing factor"];
  return [_self, "selfTempoGlide", branch, factor];
};

//TODO: implement
var tempoSteps = function (branch, target, steps) {
  if (target === undefined || steps === undefined)
    return [msg, "unable to set tempoSteps, missing parameters"];
  return [_self, "selfTempoSteps", branch, target, steps];
};

var selfReducerProc = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var toSkip = args[1];
  
  var d = new Dict("lsys_processes");
  d.replace(branch + "::reducer::isActive", 1);
  d.replace(branch + "::reducer::skipped", 0);
  var branchDict = new Dict("lsys_branch");
  var skippable = branchDict.get(branch).length;
  if(!toSkip)toSkip = skippable;
  d.replace(branch + "::reducer::toSkip", toSkip);
  d.replace(branch + '::reducer::skippable', skippable);
  
};

var selfAdderProc = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var toAdd = args[1];
  
  var d = new Dict("lsys_processes");
  d.replace(branch + "::adder::isActive", 1);
  //FIXME: nemozem inicializovat na 0, lebo neviem kolko ich je aktualne skip a kolko nie, chcelo by to niekde drzat tutu info
  
  d.replace(branch + "::adder::added", 0);
  var branchDict = new Dict("lsys_branch");
  var branchLength = branchDict.get(branch).length;
  if(toAdd === undefined)toAdd = branchLength;
  d.replace(branch + "::adder::toAdd", toAdd);
  d.replace(branch + '::adder::branchLength', branchLength);
  
};

var selfRemoveProc = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var proc = args[1];
  var d = new Dict("lsys_processes");
  d.remove(branch + "::" + proc);
};

var selfRemoveAllBranchProc = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var d = new Dict("lsys_processes");
  d.remove(branch.toString());
};

var selfFastGlide = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var factor = args[1];
  if (factor === undefined)
    return [msg, "unable to set fastGlide, missing factor"];
  var d = new Dict("lsys_processes");
  d.replace(branch + "::glide::factor", factor);
  d.replace(branch + "::glide::isActive", 1);
  d.replace(branch + "::glide::fastMode", 1);
};

var selfTempoGlide = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var factor = args[1];
  if (factor === undefined)
    return [msg, "unable to set tempoGlide, missing factor"];
  var d = new Dict("lsys_processes");
  d.replace(branch + "::tempoGlide::factor", factor);
  d.replace(branch + "::tempoGlide::isActive", 1);
};

function selfUnlockBranch(branch, proc) {
  var tree = new Dict("lsys_flat_tree_dict");
  var branches = new Dict("lsys_branch");
  var keys = branches.get(branch);

  for (var i = 0; i < keys.length; i++) {
    tree.set([keys[i]] + "::process::" + proc + "::finished", 0);
  }
}

//var MS_CONST = 250;
//var MIN_CONST = 15;

// var MS_CONST = 1000;
// var MIN_CONST = 30;

var MS_CONST = 30;
var MIN_CONST = 10;

var sec = 1;
var min = MIN_CONST * sec;

var b1 = -1;
var b2 = 70;
var b3 = 335;
var b4 = 456;
var b5 = 1930;
var b6 = 721;
var x = -1;

var ctest = [
  [msg, "INIT"],
  removeAllBranchProc(b1),

  [msg, "test"],
  resetBranch(b1),
  [wait, 2],
  [b, b1],
  fastGlide(b1, 1.0002),
  [wait, 5],
  fastGlide(b1, 1.02),
  [wait, 5],
  fastGlide(b1, 0.9),
  [wait, 10],
  fastGlide(b1, 0.999),

  [wait, 20],
  unlock(b1, glideProc),
  glide(b1, 1.1),
  decress(b1, 0.9),
  [wait, min],
  [r, b1],
  removeAllBranchProc(b1),
  [msg, "koniec testu"],
];

var tempoDownSeq = [
  [msg, "tempo down"],
   tempoGlide(b1, 1.05),  
];

var tempoUpSeq = [
  [msg, "tempo down"],
   tempoGlide(b1, 0.95),  
];

var fastUp = [
  [msg, "FAST UP"],
   fastGlide(b1, 1.3),  
];

var fastDown = [
  [msg, "FAST DOWN"],
  
   fastGlide(b1, 0.8),
  //[wait, 20],
  //fastGlide(b1, 0.5),
  [wait, 10],
    //decress(b1, 0.7),
];

var reducerSeq = [
  [msg, "reducer"],
  reducer(x, 10),
  [wait, 1],
  removeProc(x, 'reducer'),      
];

var adderSeq = [
  [msg, "adder"],
  adder(x,5),
  [wait, 1],
  removeProc(x, 'adder'),      
];



var cressSeq = [
  [msg, 'cress'],
  resetBranch(x),
  [b, x],
  calm(x),
  
  cress(x, 1.1),
    
]

var red10speed = 20;
var add10speed = 20;

var reduce10Seq = [[msg, 'reduce10']]
.concat(reducerSeq).concat([[wait,red10speed]])
.concat(reducerSeq).concat([[wait,red10speed]])
.concat(reducerSeq).concat([[wait,red10speed]])
.concat(reducerSeq).concat([[wait,red10speed]]);

var adder10Seq = [[msg, 'adder10']]
.concat(adderSeq).concat([[wait,add10speed]])
.concat(adderSeq).concat([[wait,add10speed]])
.concat(adderSeq).concat([[wait,add10speed]])
.concat(adderSeq).concat([[wait,add10speed]]);





//var motiveSeq = [[msg, 'motive'], spread(reducerSeq), [wait, 1], spread(waveSeq)];
//var motiveSeq = [[msg, 'motive']].concat(cressSeq).concat([reducer(x), [wait,10], removeProc(x, reducerProc),[wait, 10], adder(x), [wait,10]]);
var motiveSeq = [[msg, 'motive']]
.concat(cressSeq)
.concat(reduce10Seq)
.concat([removeProc(x, 'reducer'),[wait, 10]])
.concat(adder10Seq)
.concat([[wait,10], [msg,'END']]);

//var ccc = [[msg, 'motive']].concat(reduce10Seq);
//post(JSON.stringify(ccc));
//post(waveSeq);


var sequences = {
  fastDown: fastDown,
  fastUp: fastUp,
  reducerSeq:reducerSeq,
  adderSeq:adderSeq,
  motiveSeq:motiveSeq,
  reduce10Seq:reduce10Seq,
  adder10Seq:adder10Seq,
  tempoDownSeq:tempoDownSeq,
  tempoUpSeq:tempoUpSeq,
  cressSeq:cressSeq,
};

function initSequences(sequences) {
  var keys = Object.keys(sequences);
//  post(keys);
  if (!keys) return;

  for (var j = 0; j < keys.length; j++) {
    var seq = sequences[keys[j]];
    var max = seq.length;
    var songLength = 0;
    for (var i = 0; i < max; i++) {
      if (seq[i][0] === b || seq[i][0] === r) {
        seq[i].push(js);
        seq[i].push(seq[i].shift());
        seq[i].push(seq[i].shift());
      }
      // if (seq[i][0] === wait) {
      //   songLength += seq[i][1];
      //   seq[i][1] *= MS_CONST;
      // }
    }

    // post();
    // post();
    // post("======================");
    // post(keys[j]);
    // post("======================");

    for (var i = 0; i < max; i++) {
    //  post();
//      post(seq[i]);
    }

    // post();
    // post("Seq Length:");
    // post(songLength / min, "min");
  }
}

initSequences(sequences);

c = [
  [msg, "INIT"],
  [dict, processes, "clear"],
  [msg, "Skladba zacala"],
  [wait, 1],
  resetBranch(b1),
  resetBranch(b2),
  resetBranch(b3),
  resetBranch(b4),
  resetBranch(b5),
  calm(b1),
  [b, b1],
  cress(b1, 3),
  tempoGlide(b1, 1.1),
  [wait, 30],

  [wait, 10],
  tempoGlide(b1, 0.75),
  fastGlide(b1, 1.2),
  [wait, 5],
  fastGlide(b1, 1.2),
  [wait, 5],
  fastGlide(b1, 1.2),
  [wait, 5],
  fastGlide(b1, 1.2),
  tempoGlide(b1, 0.9),

  calm(b2), //B2 start
  [b, b2],
  cress(b2, 2),
  tempoGlide(b2, 0.9),
  decress(b1, 0.8),
  [wait, 20],
  cress(b2, 1.1),
  glide(b2, 1.1),
  [wait, 10],
  glide(b2, 1.05),
  [wait, 10],
  removeProc(b2, glideProc),
  decress(b2, 0.7),

  [wait, 5],
  [r, b2],

  calm(b3, 0.1),
  [b, b3],
  cress(b3, 2),
  tempoGlide(b3, 0.7),

  [msg, "pauza"],
  [wait, 10 * min],

  tempoGlide(b2, 1.001),
  tempoGlide(b1, 0.88),
  [wait, min],
  removeProc(b1, tempoGlideProc),

  [msg, "Skladba skoncila"],
];

c = [];
c = ctest;
c = fastUp;
//c = [[msg, 'INIT'],[msg, 'INIT2'],glide(b2, 1.05),[wait, 2],cress(b2, 0.5)];

//var max = c.length;
//var songLength = 0;
//add JS string
// for (var i = 0; i < max; i++) {
//   if (c[i][0] === b || c[i][0] === r) {
//     c[i].push(js);
//     c[i].push(c[i].shift());
//     c[i].push(c[i].shift());
//   }
//   if (c[i][0] === wait) {
//     songLength += c[i][1];
//     c[i][1] *= MS_CONST;
//   }
// }

// for (var i = 0; i < max; i++) {
//   post();
//   post(c[i]);
// }

// post();
// post("Song Length:");
//post(songLength / min, "min");

//post(JSON.stringify(c));

var next = function () {
  //  if (paused || stopped) return;

  if (G.SEQ < 0) return;
  if (G.SEQ >= max) return;
  G.SEQ++;
  G.sendnamed("SEQ_POSITION", "SEQ");
  //outlet(0, c[G.SEQ % max]);
  post();
  post(c[G.SEQ]);
  post();
  outlet(0, c[G.SEQ]);
  outlet(1, [G.SEQ, max]);
  outlet(2, c[G.SEQ + 1]);
  if (c[G.SEQ % max][0] !== wait) outlet(0, [wait, 1]);
};

var seqIndexses = {};
var seqStack = {};

var seqBranch = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var seq = sequences[seqStack[branch]];
  if (!seq) return;
  //post(JSON.stringify(seq));

  var seqIndex = ++seqIndexses[branch];
  if (typeof seqIndex !== "number") return;

  if (seqIndex < 0) return;
  var max = seq.length;
  if (seqIndex >= max) return;
  var step = seq[seqIndex % max].slice();

  if (step[0] !== wait && step[0] !== msg) step[2] = branch;
    else step.push(branch);
  if(step[0] === wait){
    //vypocitaj spravny cas
    step[1] *= MS_CONST;
  }
  post();
  post("\n==============\n");
  post(step);
  outlet(0, step);
  
  outlet(2, seq[seqIndex+ 1]);
  if (seq[seqIndex % max][0] !== wait) outlet(0, [wait, 1, branch]);
};

var stop = function () {
  post();
  post("-----------", stopMSG, "--------------");
  post();
  outlet(0, stopMSG);

  outlet(0, [wait, "clear"]);
  paused = false;
  stopped = true;
  G.SEQ = -1;
  G.sendnamed("SEQ_POSITION", "SEQ");
};

var pause = function () {
  paused = !paused;
  if (paused) {
    outlet(0, pauseMSG);
    return;
  }
  outlet(0, resumedMSG);
  next();
};

var start = function () {
  post();
  post();
  post();
  post();
  post();
  post();
  post();
  post("-----------", startMSG, "--------------");
  post();
  outlet(0, startMSG);
  paused = false;
  stopped = false;
  G.SEQ = 0;
  next();
};

var startSeqBranch = function () {
  var args = arrayfromargs(arguments);
  var branch = args[0];
  var seq = args[1];
  post(seq);
  seqIndexses[branch] = 0;
  seqStack[branch] = seq;

  //post(JSON.stringify(seqIndexses));
  //post(JSON.stringify(seqStack));
  //post(JSON.stringify(sequences));
  paused = false;
  stopped = false;

  seqBranch(branch);
};

var msg_int = function () {
  var args = arrayfromargs(arguments);
  outlet(1, c[args]);
};

var PP = function () {
  var a = arrayfromargs(arguments);
  var proc = a[0];
  var branch = a[1];
  var factor = a[2];
  if (proc === "glide") {
    outlet(0, glide(branch, factor));
  }
  if (proc === "fastGlide") {
    outlet(0, fastGlide(branch, factor));
  }

  if (proc === "tempoGlide") {
    outlet(0, tempoGlide(branch, factor));
  }

  if (proc === "unlock") {
    outlet(0, unlock(branch, glideProc));
  }

  if(proc ==='cress'){
    //outlet(0, calm(branch));
    outlet(0, cress(branch, factor));
  }

  if(proc ==='decress'){
    //outlet(0, resetBranch(branch));
    //outlet(0, calm(branch));
    outlet(0, decress(branch, factor));
  }

  if(proc ==='reducer'){
    outlet(0, reducer(branch, factor));
  }

  if(proc ==='adder'){
    outlet(0, adder(branch, factor));
  }

  // if(proc ==='GLOB'){
  //   outlet(0, glide('GLOB', factor));
  // }
};
