const { scaleNumber } = require("./utilities");

var TreeNode = function (id, level, children, ruleValue) {
  var n = this;
  n.id = id;
  n.level = level;
  n.ruleValue = ruleValue;
  n.children = children;
};

var R = "F[F]F";
//var R = "F[FF[FF]]FF";
//var R = 'F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF';
//var R = '+XF-YFY-FX+';
//var R = "F[FFF[F[F[F[F]]]]]F";

//var R = "FF[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]";
//+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]+[+FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]-FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]-FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]]-[-FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]+FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]+FF+[+F-F-F]-[-F+F+F]FF+[+F-F-F]-[-F+F+F]+[+FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]-FF+[+F-F-F]-[-F+F+F]]-[-FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]+FF+[+F-F-F]-[-F+F+F]]];

R = R.split("");
//var R = "FFF[F[F[F]]]F".split("");
var tree = new Array();
var R2 = [0, 1, 4, 6];

const make = function (str, level, tree) {
  var i = 0;
  var spacing = "";
  var log = function (m) {
    console.log(spacing, m ? m : "");
  };
  spacing = level;
  for (var i = 0; i < level; i++) {
    spacing = spacing + "_______";
  }

  log();
  log("NEW level: " + level);
  var r = str.join("");
  var TEST = false;
  tree = new Array();
  //tree = new TreeNode(i,level, []);
  while (str.length > 0) {
    log("i: " + i);
    log("str:  " + str.join(""));
    switch (str[0]) {
      case "F": {
        //    log(str[0]);
        tree.push(new TreeNode(i, level, []));
        log("PUSHING F");
        log(tree);
        break;
      }
      case "[": {
        str.shift();
        log("got [ ... Entering new level ");
        log("i: " + i);
        log(tree[tree.length - 1]["children"]);
        log(i);
        log("---XXX--DEBUG--XXX---");
        //tree[i].children = new Array();
        //tree.children.push(make(str, level + 1, null));
        tree[tree.length - 1]["children"].push(make(str, level + 1, null));
        break;
      }

      case "]": {
        //str.shift();
        log("got ] ... Leaving current level ");
        log("====== END of level:  " + level);

        TEST = true;
        return tree;
      }
    }
    log();
    log();
    log(
      "end of switch, string is currently: " + str.join("") + " level: " + level
    );
    i++;
    str.shift();
  }
  return tree;
};
console.log("\n \n \n");
console.log("-------------------START ALG---------------------");
//tree = make(R, 0, tree);
//console.log(JSON.stringify(tree));

var makeTree = function (rules, level, idIndex, tree, idMulti) {
  var i = 0;
  //idIndex++;
  tree = new Array();

  while (rules.length > 0) {
    switch (rules[0]) {
      case "F": {
        idIndex++;
        tree.push(new TreeNode(idIndex + idMulti * level, level, [], "F"));

        break;
      }
      case "+": {
        idIndex++;
        tree.push(new TreeNode(idIndex + idMulti * level, level, [], "+"));
        break;
      }

      case "-": {
        idIndex++;
        tree.push(new TreeNode(idIndex + idMulti * level, level, [], "-"));

        break;
      }
      case "[": {
        rules.shift();

        tree[tree.length - 1]["children"] = makeTree(
          rules,
          level + 1,
          idIndex,
          null,
          idMulti
        );

        break;
      }

      case "]": {
        return tree;
      }
    }

    i++;
    rules.shift();
  }
  return tree;
};

var R = "F[F]FF[F[F]]F";
//var R = 'F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F+[-F+F+F-F-F-F+[-F+F]+FF-F+F+F+F-F-F-F+[-F+F]+FF-F]+F+F+F-F-F-F+[-F+F]+FF-FF+F+F-F-F-F+[-F+F]+FF-F-F+F+F-F-F-F+[-F+F]+FF-F';
R = R.split("");

var tree = makeTree(R, 0, -1, tree, 10);

var searchTreeDebug = function (tree, level) {
  var spacing = "";
  spacing = level;
  for (var i = 0; i < 4 * level; i++) {
    spacing = spacing + "_______";
  }
  for (var i = 0; i < tree.length; i++) {
    console.log(spacing, "searching node: ", i, "in tree level: ", level);
    console.log(spacing, tree[i].id);
    searchTreeDebug(tree[i].children, level + 1);
  }
};

var searchTree = function (tree, level, ids) {
  if (tree.length < 1) return;

  for (var i = 0; i < tree.length; i++) {
    searchTree(tree[i].children, level + 1, ids);

    ids.push(tree[i].id);
  }
  return ids;
};

var findNodeByID = function (tree, id) {
  if (tree.length < 1) return;
  for (var i = 0; i < tree.length; i++) {
    var t = tree[i];
    if (id === t.id) return t;
    var result = findNodeByID(tree[i].children, id);
    if (result) return result;
  }
};

var getChildrenIndexes = function (tree) {
  if (tree.length < 1) return;
  for (var i = 0; i < tree.length; i++) {
    var t = tree[i];
    if (id === t.id) return t;
    var result = findNodeByID(tree[i].children, id);
    if (result) return result;
  }
};

var searchBreadth = function (tree) {
  if (tree === undefined) return;

  var ALL_NODES_COUNT = tree.length;
  var breakPoint = 0;
  var queue = [];

  for (var i = 0; i < tree.length; i++) {
    queue.push(tree[i]);
  }

  while (queue.length > 0) {
    if (breakPoint > ALL_NODES_COUNT * 10) return;
    breakPoint = breakPoint + 1;
    var currentNode = queue[0];

    if (currentNode === undefined) {
      continue;
    }
    //console.log(queue);
    //console.log(JSON.stringify(currentNode));
    console.log(currentNode);
    //outlet(1, JSON.stringify(tree[currentNode]));
    //outlet(2, tree[currentNode].id);

    for (var i = 0; i < currentNode.children.length; i++) {
      queue.push(currentNode.children[i]);
    }
    queue.shift();
  }
};

searchBreadth(tree);
// var sum = 0;
var ids = new Array();
ids = searchTree(tree[2].children, 0, ids);
console.log("ids");
console.log(ids);

var idToFind = 47;
var result = findNodeByID(tree, idToFind);
result = result ? result : "Nenasiel som id " + idToFind;

fs = require("fs");
fs.writeFile("test.json", JSON.stringify(tree), function (err) {
  if (err) return console.log(err);
  console.log("File written");
});

console.log(result);
console.log("-------------------END ALG---------------------");
console.log("-----------------------------------------------");
