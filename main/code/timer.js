//var snapshots = {};

var stack = [];

var pushToTimer = function () {
  post("ahoj");
  var args = arrayfromargs(arguments);
  var time = args[0];
  var branch = args[1];
  var snp = args[2];
  var toStack = { branch: branch, time: time, snapshot: snp };
  stack.push(toStack);
  post('timer:');
  post(JSON.stringify(stack));
};

var popTimer = function () {
  var args = arrayfromargs(arguments);
  var time = args[0];
  var newStack = [];

  for (var i = 0; i < stack.length; i++) {
    var inStack = stack[i];

    if (inStack.time + inStack.snapshot <= time) {
      outlet(0, ["seqBranch", inStack.branch]);
      continue;
    } else {
      newStack.push(inStack);
    }
  }
  stack = newStack;
};

var clear = function(){
    post('timer:');
    post(JSON.stringify(stack));
    stack = [];
    post('timer:');
  post(JSON.stringify(stack));
}

var pop = function(){
    post('timer:');
    post(JSON.stringify(stack));
}
