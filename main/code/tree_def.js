var util = require("./utilities");
var getRandomInt = util.getRandomInt;
var scaleNumber = util.scaleNumber;

// inlets = 3;
outlets = 3;

var CHILDREN_COUNT = 2;
var MAX_LEVELS_COUNT = 12;
var ROOT_FREQ = 10;
var MAX_FREQ = 16000;
var FREQ_FLUCTUATION_FACTOR = 0.1;
var BASE_DURATION = 3000;

var TreeNode = function (id, value, children) {
  node = this;
  node.id = id;
  node.level = 0;
  node.level_vertical = 0;
  node.mass = 0;
  node.height = 0; //util.getRandomInt(100);
  node.perimeter = 0;
  node.oscilating = false;
  node.value = value;

  node.duration = 0;
  node.amp = 0;
  node.baseFrequency = 0;
  node.children = children.slice();
  node.isLastNode = false;
  node.coordinates = [-1, -1];
  //node.lines = [];
  node.nodeCountOnCurrentLevel = 0;
  //node.parrent = 0;
};

var NODE_KEYES = Object.keys(new TreeNode(0, 0, []));

var initChildren = function (childrenCount, index) {
  var temp = new Array();
  for (var i = 0; i < childrenCount; i++)
    temp.push(index * childrenCount + i + 1);

  return temp;
};
//TODO: deleting, adding nodes
//TODO: use variouse children count
var deleteNodeRecursive = function (tree, id) {
  // copy root
  var newTree = tree[0];
};

var compute_freq = function () {
  var childrenCount = 2;
  var args = arrayfromargs(messagename, arguments);
  var levelsCount = args[1];
  var ffs = new Array();
  for (var depthLevel = 0; depthLevel < levelsCount; depthLevel++) {
    var nodeCountOnCurrentLevel = Math.pow(childrenCount, depthLevel);

    for (var j = 0; j < nodeCountOnCurrentLevel; j++) {
      // node.baseFrequency = randomOp(
      //   ROOT_FREQ + ROOT_FREQ * depthLevel * depthLevel,
      //   getRandomInt(ROOT_FREQ * depthLevel * FREQ_FLUCTUATION_FACTOR)
      // ) + depthLevel*(MAX_FREQ/levelsCount); //Math.pow(getRandomInt(ROOT_FREQ*FREQ_FLUCTUATION_FACTOR), depthLevel);

      //ffs.push(depthLevel * (MAX_FREQ / (levelsCount+1))); // + ROOT_FREQ +getRandomInt(ROOT_FREQ * depthLevel * FREQ_FLUCTUATION_FACTOR));
      ffs.push(util.scaleNumber(ROOT_FREQ*Math.pow(3,depthLevel), 30,16000,30,16000));
      

    }
  }

  // var rf = args[1];
  // var mf = args[2];
  // var dl = args[3];
  // var l = args[4];

  //var f = dl*(mf/(lc*lc+1))+rf;

  outlet(1, ffs);
};

var makeTreeLByLevels = function (childrenCount, levelsCount) {
  if (levelsCount > MAX_LEVELS_COUNT) levelsCount = MAX_LEVELS_COUNT;

  var tree = new Array();
  var nodeIndex = 0;
  var PLANE_WIDTH = 2500;
  for (var depthLevel = 0; depthLevel < levelsCount; depthLevel++) {
    var nodeCountOnCurrentLevel = Math.pow(childrenCount, depthLevel);
    var xFraction = PLANE_WIDTH / nodeCountOnCurrentLevel;

    for (var j = 0; j < nodeCountOnCurrentLevel; j++) {
      var node = new TreeNode(nodeIndex, 0, new Array(childrenCount));

      node.nodeCountOnCurrentLevel = nodeCountOnCurrentLevel;
      if (levelsCount - 1 > depthLevel)
        node.children = initChildren(childrenCount, nodeIndex);
      else {
        node.children = [-1, -1];
        node.isLastNode = true;
      }
      // node.baseFrequency = randomOp(
      //   ROOT_FREQ + ROOT_FREQ * depthLevel * depthLevel,
      //   getRandomInt(ROOT_FREQ * depthLevel * FREQ_FLUCTUATION_FACTOR)
      // ) + depthLevel*(MAX_FREQ/levelsCount); //Math.pow(getRandomInt(ROOT_FREQ*FREQ_FLUCTUATION_FACTOR), depthLevel);

      node.baseFrequency =
        depthLevel * (MAX_FREQ / (levelsCount * levelsCount + 1)) +
        ROOT_FREQ +
        getRandomInt(ROOT_FREQ * depthLevel * FREQ_FLUCTUATION_FACTOR);




        //node.baseFrequency = util.scaleNumber(ROOT_FREQ*Math.pow(2,depthLevel), 30,16000,30,16000);
      node.baseFrequency =
        getRandomInt(node.baseFrequency * 0.1) + node.baseFrequency;


        if(node.isLastNode){
          node.baseFrequency = MAX_FREQ + getRandomInt(MAX_FREQ*0.01)
        }

      node.duration =
        BASE_DURATION / (depthLevel * depthLevel * depthLevel * nodeIndex + 1);
      
        node.amp = 1 / (depthLevel * depthLevel + 1);

      node.level = depthLevel;
      node.value = depthLevel * 10 + depthLevel + 1;
      node.coordinates = [
        xFraction + xFraction * 2 * j,
        depthLevel * 100 + 100,
      ];

      
      nodeIndex++;
      tree.push(node);
    }
  }
  return tree;
};

// MESSAGE - getRandomNode - returns random node of tree - iteratively all the keys and their values
function getRandomNode() {
  var keys = NODE_KEYES;
  var node = tree[util.getRandomInt(tree.length)];
  var children = new Array();
  children.push("children");
  if (node.isLastNode) {
    children.push(-1);
    children.push(-1);
  } else {
    for (var i = 0; i < CHILDREN_COUNT; i++) {
      children.push(node.children[i]);
    }
  }

  outlet(0, children);
  for (var i = 0; i < keys.length; i++) {
    if (typeof node[keys[i]] !== "object") outlet(0, [keys[i], node[keys[i]]]);
  }
}

var makeTreeInternal = function (childrenCount, nodesCount) {
  var tree = new Array();
  for (var i = 0; i < nodesCount; i++) {
    var temp = new TreeNode(i, 0, new Array(childrenCount));
    temp.children = initChildren(childrenCount, i);
    tree.push(temp);
  }
  return tree;
};

var tree = makeTreeLByLevels(CHILDREN_COUNT, 6);

var makeTree = function () {
  var args = arrayfromargs(messagename, arguments);
  var childrenCount = args[1];
  var nodeCount = args[2];
  var levelsCount = args[2];
  CHILDREN_COUNT = childrenCount;
  tree = makeTreeInternal(childrenCount, nodeCount);
  //tree = makeTreeLByLevels(childrenCount, levelsCount);
  var treeDict = new Dict("tree_dict");
  treeDict.parse(JSON.stringify({ tree: tree }));
  outlet(1, "Making tree: done.");
  var freqsDict = new Dict("freqs_dict");
  var temp = new Array();
  for (var i = 0; i < tree.length; i++) {
    temp.push(tree[i].baseFrequency);
  }
  freqsDict.parse(JSON.stringify({ freqs: temp }));
};

//TASK - for async functions
// t = new Task(task, this, "CICINA");
//   t.interval = 1000; // every second
//   t.repeat(3); // do it 3 times

// var task = function (str) {
//   post(str);
// };

var applyPower = function (node, power) {};

var exciteNode = function (node, power) {
  // only for visualisation in MAX
};

var pokeNodeBreadth = function (tree, nodeId, power) {
  if (tree[nodeId] === undefined) return;
  post("Root node is: " + nodeId + "\n");
  var ALL_NODES_COUNT = tree.length;
  var breakPoint = 0;
  var queue = [];
  queue.push(nodeId);
  while (queue.length > 0) {
    if (breakPoint > ALL_NODES_COUNT) return;
    breakPoint = breakPoint + 1;
    var currentNode = queue[0];
    post("\n------\n");
    // post("Queue:");
    // post();
    // post(queue);
    post();
    post("Poking node .... " + currentNode);
    // post();
    if (tree[currentNode] === undefined) {
      continue;
    }

    //outlet(1, JSON.stringify(tree[currentNode]));
    outlet(2, tree[currentNode].id);
    if (!tree[currentNode].isLastNode) {
      for (var i = 0; i < CHILDREN_COUNT; i++) {
        if (tree[currentNode].children[i] !== -1) {
          queue.push(tree[currentNode].children[i]);
        }
      }
    }
    queue.shift();
  }

  outlet(2, "bang"); // bang to output grouped zl
  post();
  post("End of poking");
  post();
};

//post(JSON.stringify(tree[nodeId]));
// for (var i = 0; i < CHILDREN_COUNT; i++){
//   var childId = tree[nodeId].children[i];
//   post('Node ID: ',tree[childId].id, 'Power: ', power);
//   post();
// }

var poke = function () {
  var args = arrayfromargs(messagename, arguments);
  var poked = args[1];
  var power = args[2];
  if (tree[poked] === undefined) return;

  post("Poked: ", poked);
  post();

  var tempDict = new Dict("node_dict");
  tempDict.parse(JSON.stringify({ poked_node: tree[poked] }));
  exciteNode(tree[poked], power);
  //pokeNodeBreadth(tree, poked, 100);
};

//console.log(tree);

var vibrateNode = function (tree, index, power, stack) {
  if (tree[index] === undefined) return tree;
  tree[index].value = 1 + power;

  //stack.push([index, power]);
  stack.push(index);
  for (var i = 0; i < CHILDREN_COUNT; i++)
    vibrateNode(tree, tree[index].children[i], power - 10, stack);
  return stack.slice();
};

var printTreeVals = function (tree, index) {
  if (tree[index] === undefined) return;
  /* ACTION GOES HERE
        ....
    */
  for (var i = 0; i < CHILDREN_COUNT; i++)
    printTreeVals(tree, tree[index].children[i]);
};

var makeVibration = function () {
  var args = arrayfromargs(messagename, arguments);
  var node = args[1];
  var stack = new Array();
  stack = vibrateNode(tree, node, 100, stack);

  //stack = vibrateNodeWide(tree, 0, 100, stack);
  //console.log(stack);
  //console.log(stack.length);
  outlet(0, stack.slice());
  var test = { cicina: 10, kokot: 20 };
  outlet(2, JSON.stringify(test));
};

//console.log('vibrating...');
//makeVibration();

//console.log(tree);

function jsobj_to_dict(o) {
  var d = new Dict();

  for (var keyIndex in o) {
    var value = o[keyIndex];

    if (!(typeof value === "string" || typeof value === "number")) {
      var isEmpty = true;
      for (var anything in value) {
        isEmpty = false;
        break;
      }

      if (isEmpty) {
        value = new Dict();
      } else {
        var isArray = true;
        for (var valueKeyIndex in value) {
          if (isNaN(parseInt(valueKeyIndex))) {
            isArray = false;
            break;
          }
        }

        if (!isArray) {
          value = jsobj_to_dict(value);
        }
      }
    }
    d.set(keyIndex, value);
  }
  return d;
}
