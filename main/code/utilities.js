exports.scaleNumber = function (num, in_min, in_max, out_min, out_max) {
  var input_interval = in_max - in_min;
  var output_interval = out_max - out_min;
  return (num / input_interval) * output_interval + out_min;
};

exports.randomOperation = function (a, b) {
  var add = getRandomInt(2);
  if (add) return a + b;
  return a - b;
};

var getRandomInt = function (maximum) {
  return Math.floor(Math.random() * Math.floor(maximum));
};

var roughEqual = function (fixedValue, roughValue, interval) {
  if (fixedValue + fixedValue * (interval / 100) > roughValue) {
    if (fixedValue - fixedValue * (interval / 100) < roughValue) {
      return true;
    }
  }
  return false;
};

var spread = function(array){
  //post(JSON.stringify(array));
//  for(var i =0; i<array.length; i++)
  return array.slice();
}

exports.spread = spread;
exports.roughEqual = roughEqual;
exports.getRandomInt = getRandomInt;

exports.fround = function (x, p) {
  var prc = Math.pow(10, p);
  return Math.round(x * prc) / prc;
};

exports.hr = function () {
  post("\n_______________________________________________________________\n");
};

/*function post(a) {
  //console.log(a);
}*/

/**
 *
 * @param {Array} a
 * @param {number} amount
 */

exports.unsort = function (a, amount) {
  if (!a) return;
  if (!amount) amount = 100;
  var swap = function (ar, x, y) {
    var t = ar[x];
    ar[x] = ar[y];
    ar[y] = t;
  };

  for (var i = 0; i < amount; i++) {
    var index = util.getRandomInt(a.length);
    var index2 = util.getRandomInt(a.length);
    swap(a, index, index2);
  }
};

exports.getAPIparamId = function (param) {
  var device_path = "this_device parameters";

  var deviceObject = new LiveAPI(null, device_path);

  var numberOfParameters = deviceObject.children[0];
  //post("len = " + numberOfParameters + "\n");

  for (var i = 0; i < numberOfParameters; i++) {
    var param_path = device_path + " " + i;
    deviceObject = new LiveAPI(null, param_path);
    var name = deviceObject.get("name");
    //post(i + ". = " + name + "\n");
    if (name == param) return device_path + " " + i;
  }
  return null;
};

function reverseString(str) {
  var newString = "";
  for (var i = str.length - 1; i >= 0; i--) {
    newString += str[i];
  }
  return newString;
}

function ConvertNumber(x, radix) {
  //vrati cislo x v sustave radix

  if (x == 0) return 0;
  binary = "";
  while (x > 0) {
    binary = (x % radix) + binary;
    x = Math.floor(x / radix);
  }
  return binary;
}

function ConvertNumber2(x, radix) {
  //vrati cislo x v sustave radix
  if (x == 0) return 0;
  var pismena = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];
  binary = "";
  while (x > 0) {
    var temp = x % radix;
    if (temp > 9) {
      temp = pismena[temp - 10];
    }
    binary = temp + binary;
    x = Math.floor(x / radix);
  }
  return binary;
}

function ConvertIfNeeded(x, radix) {
  if (radix < 11) return x;
  return ConvertNumber2(x, radix);
}

exports.writeJson = function (name, data) {
  fs = require("fs");
  fs.writeFile(name, JSON.stringify(data), function (err) {
    if (err) return console.log(err);
    console.log("File written: " + name);
  });
};

/**--------------------------lSystem test, to delete */

/* var tree = {
  axiom: "X",
  rules: {
    //X: "F[-X][X]F[-X]+FX",
    X:"F-F+X-FFX",
    F: "FF",
    
  },
}; */

var koch = {
  axiom: " F++F++F",
  rules: {
    F: "F-F++F-F[F+F-F++[F]F]",
  },
};

var julianTree = {
  axiom: "F+F+F",
  rules: {
    F: "F-F+F",
  },
};

var glibertCurve = {
  axiom: "X",
  rules: {
    X: "-YF+XFX+FY-",
    Y: "+XF-YFY-FX+",
  },
};

var aBush = {
  axiom: "F",
  rules: {
    F: "FF+[+F-F-F]-[-F+F+F]",
  },
};

var bilina = {
  axiom: "F",
  rules: {
    //F: "F[+F]F[-F]F",
    //Katerinkin fraktalik
    F: "F+F+F-F-F-F+[-F+F]+FF-F",
  },
};

var deepTree = {
  axiom: "F",
  rules: {
    //F: "F[+F]F[-F]F",
    //Katerinkin fraktalik
    F: "F+F-F[F+F[F]F[F-F[F[F+F-F]F+F]F-F]F]F",
  },
};

// Axiom: F+F+F
// Rule: F > F-F+F
// Angle: 120

var applyNodeRule = function (char) {
  switch (char) {
    case "F":
      return new emptyNode();
  }
};

var test = function (s) {
  for (var i = 0; i < s.length; s++) {
    applyNodeRule(s[i], i);
  }
};

function applyRule(rules, char) {
  return rules[char] || char;
}

var emptyNode = function () {
  var n = this;
  n.id = 0;
  n.value = 0;
  n.leftChild = null;
  n.rightChild = null;
};

var makeL = function (n, iter) {
  if (iter > 2) return;
  n.id = iter;
  n.value = getRandomInt(100);
  n.leftChild = new emptyNode();
  n.rightChild = new emptyNode();

  makeL(n.leftChild, iter + 1);
  makeL(n.rightChild, iter + 1);
};
var makeNodes = function () {
  var x = new emptyNode();
  var iter = 0;

  makeL(x, iter);
  var dict = new Dict("ltest");
  dict.parse(JSON.stringify({ lsystem: x }));
  //post(JSON.stringify(x));
};

function renderAGeneration(system, previousGeneration) {
  var nextGeneration = "";
  for (var i = 0; i < previousGeneration.length; i++) {
    nextGeneration += applyRule(system.rules, previousGeneration[i]);
  }
  return nextGeneration;
}

numIters = 8;
system = aBush;

exports.generateLSystemString = function (numIters, system) {
  if (!system) system = aBush;
  var systemState = system.axiom;

  var all = {};
  for (var i = 1; i < numIters; i++) {
    systemState = renderAGeneration(system, systemState);
    all["gen_" + i] = systemState;
  }
  return systemState;
};
