{
	"name" : "main",
	"version" : 1,
	"creationdate" : 3699894281,
	"modificationdate" : 3704033851,
	"viewrect" : [ 0.0, 87.0, 494.0, 643.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"tree_poly_voice.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"lsystem_main.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"karplusMain.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"polySynth.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"GRAN.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"treeMultiGranullar.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tree.rezonator.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"treeSynths.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"midiMap.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{

		}
,
		"code" : 		{
			"lsystem.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"LSystemNodes.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"sequencer.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"tree_def.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"utilities.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"dict_utils.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"test.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"testLnodes.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}
,
		"other" : 		{
			"bach_gmin_2.mid" : 			{
				"kind" : "midifile",
				"local" : 1
			}
,
			"test_midi.mid" : 			{
				"kind" : "midifile",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
